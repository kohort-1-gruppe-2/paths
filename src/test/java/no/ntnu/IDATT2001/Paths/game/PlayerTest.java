package no.ntnu.IDATT2001.Paths.game;

import no.ntnu.IDATT2001.Paths.objects.details.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest{

  private Player player;

  @BeforeEach
  public void setUp() {
    List<String> inventory = new ArrayList<>();
    player = new Player("Nils Nilsen", 100, 0, 10, inventory);
    player.addToInventory("Sword ");
    player.addToInventory("Shield");
  }

  @Test
  public void testGetNameShouldReturnTrueForEquals() {

    assertEquals("Nils Nilsen", player.getName());
  }

  @Test
  public void testAddHealthShouldReturnTrueForEquals() {

    player.addHealth(50);

    assertEquals(150, player.getHealth());
  }

  @Test
  public void testAddHealthWithInvalidValue() {

    assertThrows(IllegalArgumentException.class, () -> player.addHealth(-50));

    assertEquals(100, player.getHealth());
  }

  @Test
  public void testGetHealthShouldReturnTrueForEquals() {

    assertEquals(100, player.getHealth());
  }

  @Test
  public void testAddScoreShouldReturnTrueForEquals() {

    player.addScore(50);

    assertEquals(50, player.getScore());
  }

  @Test
  public void testAddScoreWithInvalidValue() {

    assertThrows(IllegalArgumentException.class, () -> player.addScore(-50));

    assertEquals(0, player.getScore());
  }

  @Test
  public void testGetScoreShouldReturnTrueForEquals() {

    assertEquals(0, player.getScore());
  }

  @Test
  public void testAddGoldShouldReturnTrueForEquals() {

    player.addGold(20);

    assertEquals(30, player.getGold());
  }

  @Test
  public void testAddGoldWithInvalidValue() {

    assertThrows(IllegalArgumentException.class, () -> player.addGold(-20));

    assertEquals(10, player.getGold());
  }

  @Test
  public void testGetGoldShouldReturnTrueForEquals() {
    assertEquals(10, player.getGold());
  }

  @Test
  public void testAddInventoryShouldReturnTrue() {

    player.addToInventory("Potion");

    assertTrue(player.getInventory().contains("potion"));
  }

  @Test
  public void testAddInventoryWithEmptyItem() {

    assertThrows(NullPointerException.class, () -> player.addToInventory(""));

    assertEquals(2, player.getInventory().size());
  }

  @Test
  public void testGetInventory() {

    List<String> expectedInventory = new ArrayList<>();

    expectedInventory.add("sword");
    expectedInventory.add("shield");

    assertEquals(expectedInventory, player.getInventory());
  }
}
