package no.ntnu.IDATT2001.Paths.game;

import no.ntnu.IDATT2001.Paths.objects.action.*;
import no.ntnu.IDATT2001.Paths.objects.details.Link;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class LinkTest {

  private Link attack, run, keepRunning, cast, sword, gold;
  private List<Action> moreGold, attackTroll, keepOnRunning, magicSpell, useTheSword;


  @BeforeEach
  public void setUp() {
    moreGold = new ArrayList<>();
    GoldAction goldAction = new GoldAction();
    goldAction.GoldAction(100);
    moreGold.add(goldAction);

    attackTroll = new ArrayList<>();
    GoldAction secondGoldAction = new GoldAction();
    secondGoldAction.GoldAction(50);
    attackTroll.add(secondGoldAction);
    HealthAction healthAction = new HealthAction();
    healthAction.HealthAction(50);
    attackTroll.add(healthAction);

    keepOnRunning = new ArrayList<>();
    InventoryAction inventoryAction = new InventoryAction();
    inventoryAction.InventoryAction("potion");
    keepOnRunning.add(inventoryAction);

    magicSpell = new ArrayList<>();

    useTheSword = new ArrayList<>();
    ScoreAction scoreAction = new ScoreAction();
    scoreAction.ScoreAction(40);
    useTheSword.add(scoreAction);


    run = new Link("Run away", "You have ran 100 meters", moreGold);
    attack = new Link("Attack the troll", "The troll got attacked", attackTroll);
    keepRunning = new Link("Keep running", "You have ran 200 meters", keepOnRunning);
    cast = new Link("Cast magic spell", "Magic spell got casted", magicSpell);
    sword = new Link("Use sword", "You used the sword", useTheSword);
    gold = new Link("Take gold coins", "Gold coins collected", moreGold);
  }

  @Test
  public void testEqualsWithDifferentActions() {
    Link runCopy = new Link("Run away", "You have ran 100 meters", attackTroll);
    assertEquals(run, runCopy);
  }

  @Test
  public void testHashCodeWithSameActions() {
    Link runCopy = new Link("Run away", "You have ran 100 meters", moreGold);
    assertEquals(run.hashCode(), runCopy.hashCode());
  }

  @Test
  public void testGetTextReturnsExpectedText() {
    assertEquals("Run away", run.getText());
    assertEquals("Attack the troll", attack.getText());
    assertEquals("Keep running", keepRunning.getText());
    assertEquals("Cast magic spell", cast.getText());
    assertEquals("Use sword", sword.getText());
    assertEquals("Take gold coins", gold.getText());
  }

  @Test
  public void testGetReferenceMethodToSeeIfItReturnsCorrectReference() {
    assertEquals("You have ran 100 meters", run.getReference());
    assertEquals("The troll got attacked", attack.getReference());
    assertEquals("You have ran 200 meters", keepRunning.getReference());
    assertEquals("Magic spell got casted", cast.getReference());
    assertEquals("You used the sword", sword.getReference());
    assertEquals("Gold coins collected", gold.getReference());
  }

  @Test
  public void testGetActionsReturnsExpectedReference() {
    assertEquals(moreGold, run.getActions());
    assertEquals(attackTroll, attack.getActions());
    assertEquals(keepOnRunning, keepRunning.getActions());
    assertEquals(magicSpell, cast.getActions());
    assertEquals(useTheSword, sword.getActions());
    assertEquals(moreGold, gold.getActions());
  }

  @Test
  public void testEqualsShouldReturnTrueWhenLinksAreEqual() {
    Link runCopy = new Link("Run away", "You have ran 100 meters", moreGold);

    assertEquals(run, runCopy);
  }
  @Test
  public void testEqualsShouldReturnFalseWhenLinksAreEqual() {

    assertNotEquals(run, attack);
    assertNotEquals(run, keepRunning);
  }

  @Test
  public void testHashCodeShouldReturnSameHashCodeWhenLinksAreEqual() {
    Link runCopy = new Link("Run away", "You have ran 100 meters", moreGold);
    assertEquals(run.hashCode(), runCopy.hashCode());
  }
}
