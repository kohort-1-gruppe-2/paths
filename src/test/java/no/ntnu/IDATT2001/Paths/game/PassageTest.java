package no.ntnu.IDATT2001.Paths.game;

import no.ntnu.IDATT2001.Paths.exceptions.CommaInNameException;
import no.ntnu.IDATT2001.Paths.objects.details.Link;
import no.ntnu.IDATT2001.Paths.objects.details.Passage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class PassageTest {
  private Passage passage;
  private List<Link> links;

  @BeforeEach
  public void setUp() {
    links = new ArrayList<>();
    Link attack = new Link("Attack the troll", "Attack the troll", new ArrayList<>());
    Link run = new Link("Run away", "Run away", new ArrayList<>());
    links.add(attack);
    links.add(run);

    passage = new Passage("You see a troll", "You walk in to the forest and see a troll", links);
  }

  @Test
  public void testGetTitleToCheckIfCorrectTitleGetsReturned() {
    String expectedTitle = "You see a troll";
    String actualTitle = passage.getTitle();
    assertEquals(expectedTitle, actualTitle);
  }

  @Test
  public void testGetContentToCheckIfCorrectContentGetsReturned() {
    String expectedContent = "You walk in to the forest and see a troll";
    String actualContent = passage.getContent();
    assertEquals(expectedContent, actualContent);
  }

  @Test
  public void testAddLinkMethodToCheckIfLinkGetsAdded() {
    Link keepRunning = new Link("Keep running", "Keep running", new ArrayList<>());
    assertTrue(passage.addLink(keepRunning));
    assertTrue(passage.getLinks().contains(keepRunning));
  }

  @Test
  public void testPassageConstructorWithEmptyNameShouldThrowException() {
    String title = "";
    String content = "This is a passage with an empty title.";
    List<Link> links = new ArrayList<>();

    assertThrows(IllegalArgumentException.class, () -> new Passage(title, content, links));
  }

  @Test
  public void testPassageConstructorWithNullAsNameShouldThrowException() {
    String title = null;
    String content = "This is a passage with null as title.";
    List<Link> links = new ArrayList<>();

    assertThrows(NullPointerException.class, () -> new Passage(title, content, links));
  }

  @Test
  public void testPassageConstructorWithCommaInNameShouldThrowException() {
    String title = "Passage,WithComma";
    String content = "This is a passage with a comma in the title.";
    List<Link> links = new ArrayList<>();

    assertThrows(CommaInNameException.class, () -> new Passage(title, content, links));
  }

  @Test
  public void testAddExistingLinkToCheckIfItReturnsFalseWhenAddedAlreadyExistingLink() {
    Link existingLink = links.get(0);
    assertFalse(passage.addLink(existingLink));
    assertTrue(passage.getLinks().contains(existingLink));
  }

  @Test
  public void testGetLinksToCheckIfItReturnsCorrectLink() {
    List<Link> expectedLinks = links;
    List<Link> actualLinks = passage.getLinks();
    assertEquals(expectedLinks, actualLinks);
  }

  @Test
  public void testHasLinksToCheckIfItReturnsFalseWhenLinkDoesNotExist() {
    Link existingLink = links.get(0);
    assertTrue(passage.hasLinks(existingLink));

    Link nonExistingLink = new Link("Non-existing Link", "Non-existing Reference", new ArrayList<>());
    assertFalse(passage.hasLinks(nonExistingLink));
  }

  @Test
  public void testEqualsWithSamePassageObjectsShouldReturnTrue() {
    Passage samePassage = new Passage("You see a troll", "You walk in to the forest and see a troll", links);
    assertEquals(passage, samePassage);
  }
  @Test
  public void testEqualsWithSamePassageObjectsShouldReturnFalse() {

    Passage differentPassage = new Passage("Different Title", "Different Content", links);
    assertNotEquals(passage, differentPassage);
  }


  @Test
  public void testHashCodeWithEqualPassageObjectsShouldReturnEqualHashCodes() {
    Passage samePassage1 = new Passage("Title", "Content", links);
    Passage samePassage2 = new Passage("Title", "Content", links);

    assertEquals(samePassage1.hashCode(), samePassage2.hashCode());
  }

}
