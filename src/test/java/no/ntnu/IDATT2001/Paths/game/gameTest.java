package no.ntnu.IDATT2001.Paths.game;

import no.ntnu.IDATT2001.Paths.objects.details.*;
import no.ntnu.IDATT2001.Paths.objects.goal.Goal;
import no.ntnu.IDATT2001.Paths.objects.goal.GoldGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {
  private Player player;
  private Story troll;
  private List<Goal> goals;
  private Game game;
  private Passage passage1, passage2, passage3, passage4, passage5, passage6;
  private Passage openingPassage;
  private GoldGoal goldGoal;
  Map<Link, Passage> passages;



  @BeforeEach
  void setUp() {
    player = new Player("John", 100, 0, 10, new ArrayList<>());
    goals = new ArrayList<>();
    goldGoal = new GoldGoal();
    goldGoal.GoldGoal(100);
    goals.add(goldGoal);
    Link link = new Link("Run away", "Run away", new ArrayList<>());
    ArrayList<Link> links= new ArrayList<>();
    links.add(link);
    openingPassage = new Passage("You see a troll", "You walk in to the forest and see a troll", new ArrayList<>());
    passage1 = new Passage("Attack the troll", "You can attack the troll if you want to", new ArrayList<>());
    passage2 = new Passage("Run away", "Here is your chance to run away", links);
    passage3 = new Passage("Use sword", "The sword only tickles the troll. It rips the sword from your hand and stabs you to death", new ArrayList<>());
    passage4 = new Passage("Keep running", "You are too slow. The troll catches up to you, and hits you in the head. it is a fatal blow", new ArrayList<>());
    passage5 = new Passage("Cast magic spell", "the troll turns to stone. 50 gold coins drop to the ground",new ArrayList<>());
    passage6 = new Passage("Take gold coins", "You have earned 50 gold coins", new ArrayList<>());
    passages = new HashMap<>();
    troll = new Story("Troll attack", openingPassage, passages);
    troll.addPassage(passage1);
    troll.addPassage(passage2);
    troll.addPassage(passage3);
    troll.addPassage(passage4);
    troll.addPassage(passage5);
    troll.addPassage(passage6);
    game = new Game(player, troll, goals);


  }

  @Test
  void getPlayerShouldReturnPlayer() {
    assertEquals(player, game.getPlayer());
  }

  @Test
  void getStoryShouldReturnStory() {
    assertEquals(troll, game.getStory());
  }

  @Test
  void getGoalsShouldReturnGoals() {
    assertEquals(goals, game.getGoals());
  }

  @Test
  void beginShouldReturnOpeningPassage() {

    assertEquals(openingPassage, game.begin());
  }

  @Test
  void goShouldReturnPassage() {

    assertEquals(passage2, game.go(passage2.getLinks().get(0)));
  }


}

