package no.ntnu.IDATT2001.Paths.game;


import no.ntnu.IDATT2001.Paths.objects.details.Link;
import no.ntnu.IDATT2001.Paths.objects.details.Passage;
import no.ntnu.IDATT2001.Paths.objects.details.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class StoryTest {

  private Story troll;
  private Passage openingPassage;
  private Passage passage1, passage2, passage3, passage4, passage5, passage6;
  Map<Link, Passage> passages;




  @BeforeEach
  public void setUp() {


    openingPassage = new Passage("You see a troll", "You walk in to the forest and see a troll", new ArrayList<>());
    passage1 = new Passage("Attack the troll", "You can attack the troll if you want to", new ArrayList<>());
    passage2 = new Passage("Run away", "Here is your chance to run away", new ArrayList<>());
    passage3 = new Passage("Use sword", "The sword only tickles the troll. It rips the sword from your hand and stabs you to death", new ArrayList<>());
    passage4 = new Passage("Keep running", "You are too slow. The troll catches up to you, and hits you in the head. it is a fatal blow", new ArrayList<>());
    passage5 = new Passage("Cast magic spell", "the troll turns to stone. 50 gold coins drop to the ground",new ArrayList<>());
    passage6 = new Passage("Take gold coins", "You have earned 50 gold coins", new ArrayList<>());
    passages = new HashMap<>();
    troll = new Story("Troll attack", openingPassage, passages);
    troll.addPassage(passage1);
    troll.addPassage(passage2);
    troll.addPassage(passage3);
    troll.addPassage(passage4);
    troll.addPassage(passage5);
    troll.addPassage(passage6);


  }



  @Test
  public void testGetBrokenLinksReturnsCorrectBrokenLinks() {
    List<Link> brokenLinks = troll.getBrokenLinks();
    assertEquals(0, brokenLinks.size());

    Passage passage7 = new Passage("Passage 7", "This is passage 7.", new ArrayList<>());
    Link brokenLink = new Link("Broken Link", "Non-existent Passage", new ArrayList<>());
    passage7.addLink(brokenLink);

    troll.addPassage(passage7);
    brokenLinks = troll.getBrokenLinks();

    assertEquals(1, brokenLinks.size());
    assertEquals(brokenLink, brokenLinks.get(0));
  }

  @Test
  public void testAddPassageCorrectlyAddsOnlyWhenThereIsNoDuplicates() {
    Passage passage7 = new Passage("Passage 7", "This is passage 7.", new ArrayList<>());

    assertTrue(troll.addPassage(passage7));
    assertFalse(troll.addPassage(passage7));
  }

  @Test
  public void testGetPassageToCheckIfCorrectPassageGetsReturned() {
    Link link = new Link("Use sword", "Use sword", new ArrayList<>());
    Passage retrievedPassage = troll.getPassage(link);

    assertEquals("Use sword", retrievedPassage.getTitle());
    assertEquals("The sword only tickles the troll. It rips the sword from your hand and stabs you to death", retrievedPassage.getContent());
  }

  @Test
  public void testGetPassagesToCheckIfCorrectPassagesGetsReturned() {
    Collection<Passage> passages = troll.getPassages();

    assertEquals(6, passages.size());
    assertTrue(passages.contains(passage1));
    assertTrue(passages.contains(passage2));
    assertTrue(passages.contains(passage3));
    assertTrue(passages.contains(passage4));
    assertTrue(passages.contains(passage5));
    assertTrue(passages.contains(passage6));


  }

  @Test
  public void testRemovePassageThatHasOtherPassagesConnectedReturnFalse() {

    Link link = new Link("This is the way to passage 1", "Passage 1", new ArrayList<>());

    assertFalse(troll.removePassage(link));
  }
  @Test
  public void testRemovePassageThatDoesExistShouldReturnTrue() {
    Link run = new Link("Run away", "Run away", new ArrayList<>());

    assertTrue(troll.removePassage(run));
    assertFalse(troll.getPassages().contains(passage2));
  }


  @Test
  public void testStoryConstructorTestToCheckIfExceptionsGetsThrown() {
    assertEquals("Troll attack", troll.getTitle());
    assertEquals(openingPassage, troll.getOpeningPassage());

    String emptyTitle = "";
    assertThrows(NullPointerException.class, () -> new Story(emptyTitle, openingPassage, passages));

    assertThrows(NullPointerException.class, () -> new Story(null, null, passages));
  }


}
