package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.objects.action.ScoreAction;
import no.ntnu.IDATT2001.Paths.objects.details.Player;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ScoreActionTest {
  private ScoreAction scoreAction;
  private Player nonPlayer;
  private Player player;

  @BeforeEach
  public void setup() {
    scoreAction = new ScoreAction();
    player = new Player("John", 100, 0, 0, new ArrayList<>());
    nonPlayer = null;
  }


  @Test
  public void testExecuteWithPlayerThatIsNullShouldReturnFalse() {
    int points = 50;

    scoreAction.ScoreAction(points);

    Assertions.assertFalse(scoreAction.execute(nonPlayer));
    Assertions.assertFalse(nonPlayer != null && nonPlayer.getInventory().contains(points));
  }

  @Test
  public void testScoreActionWithValidPointsShouldChangePlayerScoreAndReturnTrue() {
    int points = 50;

    assertTrue(scoreAction.ScoreAction(points));
    assertTrue(scoreAction.execute(player));
    assertEquals(points, player.getScore());
  }

  @Test
  public void testScoreActionWithZeroPointsShouldNotChangePlayerScoreAndReturnFalse() {
    int points = 0;

    Assertions.assertFalse(scoreAction.ScoreAction(points));
    assertThrows(IllegalArgumentException.class, () -> scoreAction.execute(player));
    assertEquals(0, player.getScore());
  }

  @Test
  public void testScoreActionWithNegativePointsShouldNotChangePlayerScoreAndReturnFalse() {
    int points = -50;

    Assertions.assertFalse(scoreAction.ScoreAction(points));
    assertEquals(0, player.getScore());
  }

  @Test
  public void testGetScoreAmountShouldReturnCorrectPoints() {
    int points = 100;

    scoreAction.ScoreAction(points);
    assertEquals(points, scoreAction.getScoreAmount());
  }
}
