package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.objects.action.GoldAction;
import no.ntnu.IDATT2001.Paths.objects.details.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class GoldActionTest {

  private GoldAction goldAction;
  private Player player;

  @BeforeEach
  public void setup() {
    int goldAmount = 50;
    player = new Player("John", 100, 0, 0, new ArrayList<>());
    goldAction = new GoldAction();
    goldAction.GoldAction(goldAmount);
  }

  @Test
  public void testExecuteWhenPlayerNotNullChangesPlayerGold() {
    int initialGold = player.getGold();
    int expectedGold = initialGold + goldAction.getGoldAmount();

    boolean result = goldAction.execute(player);

    assertTrue(result, "GoldAction execution should return true");
    assertEquals(expectedGold, player.getGold(), "Player's gold should be increased by the gold action amount");
  }

  @Test
  public void testExecuteWhenPlayerIsNullReturnsFalse() {
    Player nullPlayer = null;

    boolean result = goldAction.execute(nullPlayer);

    assertFalse(result, "GoldAction execution should return false when player is null");
  }

  @Test
  public void testGetGoldAmountReturnsCorrectGoldAmount() {
    int expectedGoldAmount = 50;

    int goldAmount = goldAction.getGoldAmount();

    assertEquals(expectedGoldAmount, goldAmount, "GoldAction's gold amount should be returned correctly");
  }
  @Test
  public void testExecuteWhenGoldIsNegativeReturnsFalse() {
    int negativeGold = -50;
    boolean negativeGoldAmountTest = goldAction.GoldAction(negativeGold);

    goldAction.execute(player);

    assertFalse(negativeGoldAmountTest, "GoldAction execution should return false when gold amount is negative");
    assertEquals(50, player.getGold(), "Player's gold should remain unchanged when gold amount is negative");  }

}
