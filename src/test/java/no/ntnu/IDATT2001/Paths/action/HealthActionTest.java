package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.objects.action.HealthAction;
import no.ntnu.IDATT2001.Paths.objects.details.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class HealthActionTest {
  private Player player;

  @BeforeEach
  public void setUp() {
    player = new Player("John", 100, 0, 0, new ArrayList<>());
  }

  @Test
  public void testExecuteWhenHealthIsPositiveReturnsTrueAndUpdatesPlayerHealth() {
    int positiveHealth = 50;
    HealthAction healthAction = new HealthAction();
    healthAction.HealthAction(positiveHealth);

    boolean result = healthAction.execute(player);

    assertTrue(result, "HealthAction execution should return true when health is positive");
    assertEquals(150, player.getHealth(), "Player's health should be updated to the new value with the old");
  }
  @Test
  public void testExecuteWhenHealthIsNegativeReturnsFalseAndDoesNotUpdatePlayerHealth() {
    int negativeHealth = -50;
    HealthAction healthAction = new HealthAction();

    boolean result = healthAction.HealthAction(negativeHealth);

    assertFalse(result, "HealthAction execution should return false when health is negative");
    assertEquals(100, player.getHealth(), "Player's health should not be changed when health is negative");
  }
  @Test
  public void testExecuteWhenHealthIsZeroReturnsFalseAndDoesNotUpdatePlayerHealth() {
    int zeroHealth = 0;
    HealthAction healthAction = new HealthAction();
    healthAction.HealthAction(zeroHealth);

    boolean result = healthAction.execute(player);

    assertTrue(result, "HealthAction execution should return true when health is zero");
    assertEquals(100, player.getHealth(), "Player's health should stay the same");
  }


}
