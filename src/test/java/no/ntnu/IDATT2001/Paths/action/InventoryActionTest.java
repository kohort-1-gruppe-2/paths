package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.objects.action.InventoryAction;
import no.ntnu.IDATT2001.Paths.objects.details.Player;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class InventoryActionTest {
  private Player player;
  private Player nonPlayer;
  private InventoryAction inventoryAction;

  @Before
  public void setUp() {
    player = new Player("John", 100, 0, 0, new ArrayList<>());
    nonPlayer = null;
    inventoryAction = new InventoryAction();
  }

  @Test
  public void testExecuteWithValidPlayerAndNonEmptyItemShouldAddToInventoryAndReturnTrue() {
    String item = "Sword";

    inventoryAction.InventoryAction(item);
    inventoryAction.execute(player);

    assertEquals(1, player.getInventory().size());
    assertTrue(inventoryAction.execute(player));
    assertTrue(inventoryAction.execute(player));
    assertTrue(player.getInventory().contains(item.toLowerCase()));
  }


  @Test
  public void testExecuteWithValidPlayerAndEmptyItemShouldNotAddToInventoryAndReturnFalse() {
    String item = "";

    assertFalse(inventoryAction.InventoryAction(item));
    assertFalse(player.getInventory().contains(""));
  }


}
