package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;
import no.ntnu.IDATT2001.Paths.objects.goal.HealthGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class HealthGoalTest {

  private HealthGoal healthGoal;
  private Player player;

  @BeforeEach
  public void setup() {
    int minimumHealth = 50;
    int playerHealth = 0;
    player = new Player("John", playerHealth, 0, 0, new ArrayList<>());
    healthGoal = new HealthGoal();
    healthGoal.healthGoal(minimumHealth);
  }

  @Test
  public void testIsFulfilledWhenPlayerHasEqualHealthReturnsTrue() {
    player.addHealth(50);

    boolean isFulfilled = healthGoal.isFulfilled(player);

    assertTrue(isFulfilled, "Health goal should be fulfilled when player has equal health");
  }

  @Test
  public void testIsFulfilledWhenPlayerHasGreaterHealthReturnsTrue() {
    player.addHealth(100);

    boolean isFulfilled = healthGoal.isFulfilled(player);

    assertTrue(isFulfilled, "Health goal should be fulfilled when player has greater health");
  }

  @Test
  public void testIsFulfilledWhenPlayerHasLessHealthReturnsFalse() {
    player.addHealth(30);

    boolean isFulfilled = healthGoal.isFulfilled(player);

    assertFalse(isFulfilled, "Health goal should not be fulfilled when player has less health");
  }

}
