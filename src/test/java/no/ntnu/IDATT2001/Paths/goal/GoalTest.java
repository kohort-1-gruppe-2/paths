package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;
import no.ntnu.IDATT2001.Paths.objects.goal.Goal;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GoalTest {

  @Test
  void isFulfilled_ReturnsTrue_WhenPlayerFulfillsGoal() {
    Player player = new Player("John", 100, 500, 10, new ArrayList<>());
    Goal goal = p -> p.getHealth() >= 100 && p.getScore() >= 500 && p.getGold() >= 10;

    boolean result = goal.isFulfilled(player);

    assertTrue(result);
  }

  @Test
  void isFulfilled_ReturnsFalse_WhenPlayerDoesNotFulfillGoal() {
    Player player = new Player("Alice", 80, 300, 5, new ArrayList<>());
    Goal goal = p -> p.getHealth() >= 100 && p.getScore() >= 500 && p.getGold() >= 10;

    boolean result = goal.isFulfilled(player);

    assertFalse(result);
  }
}
