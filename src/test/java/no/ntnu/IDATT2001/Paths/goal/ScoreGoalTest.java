package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;
import no.ntnu.IDATT2001.Paths.objects.goal.ScoreGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ScoreGoalTest {

  private ScoreGoal scoreGoal;
  private Player player;

  @BeforeEach
  public void setup() {
    int minimumPoints = 100;
    player = new Player("John", 100, 0, 0, new ArrayList<>());
    scoreGoal = new ScoreGoal();
    scoreGoal.ScoreGoal(minimumPoints);
  }

  @Test
  public void testIsFulfilled_WhenPlayerHasMinimumPoints_ReturnsTrue() {
    player.addScore(100);

    boolean isFulfilled = scoreGoal.isFulfilled(player);

    assertTrue(isFulfilled, "Score goal should be fulfilled when player has minimum points");
  }

  @Test
  public void testIsFulfilled_WhenPlayerHasLessThanMinimumPoints_ReturnsFalse() {
    player.addScore(50);

    boolean isFulfilled = scoreGoal.isFulfilled(player);

    assertFalse(isFulfilled, "Score goal should not be fulfilled when player has less than minimum points");
  }

  @Test
  public void testIsFulfilled_WhenPlayerHasMoreThanMinimumPoints_ReturnsTrue() {
    player.addScore(200);

    boolean isFulfilled = scoreGoal.isFulfilled(player);

    assertTrue(isFulfilled, "Score goal should be fulfilled when player has more than minimum points");
  }
}
