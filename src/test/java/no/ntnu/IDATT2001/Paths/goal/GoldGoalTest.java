package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;
import no.ntnu.IDATT2001.Paths.objects.goal.GoldGoal;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class GoldGoalTest {

  @Test
  void isFulfilled_ReturnsTrue_WhenPlayerHasMinimumGold() {
    Player player = new Player("John", 100, 500, 10, new ArrayList<>());
    int minimumGold = 10;
    GoldGoal goldGoal = new GoldGoal();
    goldGoal.GoldGoal(minimumGold);

    boolean result = goldGoal.isFulfilled(player);

    assertTrue(result);
  }

  @Test
  void isFulfilled_ReturnsFalse_WhenPlayerDoesNotHaveMinimumGold() {
    Player player = new Player("Alice", 80, 300, 5, new ArrayList<>());
    int minimumGold = 10;
    GoldGoal goldGoal = new GoldGoal();
    goldGoal.GoldGoal(minimumGold);

    boolean result = goldGoal.isFulfilled(player);

    assertFalse(result);
  }
}
