package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;
import no.ntnu.IDATT2001.Paths.objects.goal.InventoryGoal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class InventoryGoalTest {

  private InventoryGoal inventoryGoal;
  private Player player;

  @BeforeEach
  public void setup() {
    List<String> mandatoryItems = Arrays.asList("sword", "shield");
    player = new Player("John", 100, 0, 0, new ArrayList<>());
    inventoryGoal = new InventoryGoal();
    inventoryGoal.InventoryGoal(mandatoryItems);
  }

  @Test
  public void testIsFulfilled_WhenPlayerHasAllMandatoryItems_ReturnsTrue() {
    player.addToInventory("sword");
    player.addToInventory("shield");

    boolean isFulfilled = inventoryGoal.isFulfilled(player);

    assertTrue(isFulfilled, "Inventory goal should be fulfilled when player has all mandatory items");
  }

  @Test
  public void testIsFulfilled_WhenPlayerHasMissingMandatoryItems_ReturnsFalse() {
    player.addToInventory("sword");

    boolean isFulfilled = inventoryGoal.isFulfilled(player);

    assertFalse(isFulfilled, "Inventory goal should not be fulfilled when player is missing mandatory items");
  }

  @Test
  public void testIsFulfilled_WhenPlayerHasExtraItems_ReturnsTrue() {
    player.addToInventory("sword");
    player.addToInventory("shield");
    player.addToInventory("potion");

    boolean isFulfilled = inventoryGoal.isFulfilled(player);

    assertTrue(isFulfilled, "Inventory goal should be fulfilled when player has extra items");
  }
}
