package no.ntnu.IDATT2001.Paths.data;

import no.ntnu.IDATT2001.Paths.exceptions.CommaInNameException;
import no.ntnu.IDATT2001.Paths.exceptions.FileNotPathException;
import no.ntnu.IDATT2001.Paths.exceptions.InvalidFormatException;
import no.ntnu.IDATT2001.Paths.objects.action.Action;
import no.ntnu.IDATT2001.Paths.objects.action.HealthAction;
import no.ntnu.IDATT2001.Paths.objects.data.DataController;
import no.ntnu.IDATT2001.Paths.objects.details.Link;
import no.ntnu.IDATT2001.Paths.objects.details.Passage;
import no.ntnu.IDATT2001.Paths.objects.details.Story;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;

class DataControllerTest {

  private DataController writeDataController, readDataController;
  private String exampleWritePath = "/Users/abhiramikarunananthan/Desktop/Paths/src/test/java/no/ntnu/IDATT2001/Paths/data/example.paths";
  private String exampleReadPath = "/Users/abhiramikarunananthan/Desktop/Paths/src/test/java/no/ntnu/IDATT2001/Paths/data/exampleRead.paths";

  @BeforeEach
  void setUp() {
    try {
      File exampleReadFile = new File(exampleReadPath);
      File exampleWriteFile = new File(exampleWritePath);
      readDataController = new DataController(exampleReadFile);
      writeDataController = new DataController(exampleWriteFile);
    } catch (FileNotPathException e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testConstructorWithValidFile() {
    assertEquals("exampleRead.paths", readDataController.getFileName());
    assertNotNull(readDataController.getFile());
  }

  @Test
  public void testWriteStoryToFile() {
    HealthAction action = new HealthAction();
    action.HealthAction(50);
    ArrayList<Action> listOfActions = new ArrayList<>();
    listOfActions.add(action);
    Link link = new Link("Passage 1", "Passage 1", listOfActions);

    Passage passage1 = new Passage("Passage 1", "Content of passage 1", new ArrayList<>());
    Passage passage2 = new Passage("Passage 2", "Content of passage 2", new ArrayList<>());
    Passage passage3 = new Passage("Passage 3", "Content of passage 3", new ArrayList<>());
    HashMap<Link, Passage> passages = new HashMap<>();
    Story story = new Story("Sample Story", passage1, passages);

    story.addPassage(passage2);
    story.addPassage(passage3);


    try {
      File exampleFile = new File(exampleWritePath); // Use exampleFilePath when writing the file
      boolean result = writeDataController.writeStoryToFile(story);
      assertTrue(result);

      File readExampleFile = new File(exampleReadPath); // Use exampleReadPath when reading the file
      Story readStory = writeDataController.readStoryFromFile();
      assertEquals(story.getTitle(), readStory.getTitle());
      assertEquals(story.getOpeningPassage().getTitle(), readStory.getOpeningPassage().getTitle());
      assertEquals(story.getPassages().size(), readStory.getPassages().size());
    } catch (IOException | InvalidFormatException | CommaInNameException e) {
      e.printStackTrace();
      fail("Exception occurred: " + e.getMessage());
    }
  }

    @Test
    public void testReadStoryFromFile() {
      try {
        File readExampleFile = new File(exampleWritePath);
        Story readStory = readDataController.readStoryFromFile();

        assertEquals("Haunted House", readStory.getTitle());
        assertEquals("Beginnings", readStory.getOpeningPassage().getTitle());
        assertEquals(1, readStory.getOpeningPassage().getLinks().size());
        assertEquals("Another room", readStory.getOpeningPassage().getLinks().get(0).getReference());
      } catch (IOException | InvalidFormatException | CommaInNameException e) {
        e.printStackTrace();
        fail("Exception occurred: " + e.getMessage());
      }
    }
  }
