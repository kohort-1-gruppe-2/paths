package no.ntnu.IDATT2001.Paths.game;

import no.ntnu.IDATT2001.Paths.goal.Goal;

import java.util.List;

public class Game {
  private Player player;
  private Story story;
  private List<Goal> goals;

  public Player getPlayer() {
    return player;
  }

  public Story getStory() {
    return story;
  }

  public List<Goal> getGoals() {
    return goals;
  }

  public Passage begin(){
    return story.getOpeningPassage();
  }

  public Passage go(Link link) {
    return story.getPassage(link);

  }
}
