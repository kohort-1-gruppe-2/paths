package no.ntnu.IDATT2001.Paths.game;

import no.ntnu.IDATT2001.Paths.action.Action;
import java.util.List;

public class Link {
  private String text;
  private String reference;
  private List<Action> actions;

  public Link(String text, String reference, List<Action> actions) {
    this.text = text;
    this.reference = reference;
    this.actions = actions;
  }

  public String getText() {
    return text;
  }

  public String getReference() {
    return reference;
  }

  public List<Action> getActions() {
     return actions;
  }



  @Override
  public String toString() {
    return "Link: " +
            "text: " + text +
            "reference: " + reference +
            "actions: " + actions;
  }



}



