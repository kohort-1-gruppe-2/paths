package no.ntnu.IDATT2001.Paths.game;

import java.util.List;
import java.util.Objects;

public class Passage {
  private String title;
  private String content;
  private List<Link> links;


  public Passage(String title, String content, List<Link> links) {
    this.title = title;
    this.content = content;
    this.links = links;
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }

  public boolean addLink(Link link){
    if (links.contains(link)) {
      return false;
    } else {
      links.add(link);
      return true;
    }

  }

  public List<Link> getLinks() {
    return links;
  }

  public boolean hasLinks(Link link){
    return links.contains(link);
  }



  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Passage passage)) return false;
    return Objects.equals(title, passage.title) && Objects.equals(content, passage.content) && Objects.equals(links, passage.links);
  }

  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }
}

