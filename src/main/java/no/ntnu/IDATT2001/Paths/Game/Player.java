package no.ntnu.IDATT2001.Paths.game;

import java.util.List;

public class Player {
  private String name;
  private int health;
  private int score;
  private int gold;
  private List<String> inventory;

  public Player(String name, int health, int score, int gold, List<String> inventory) {
    this.name = name;
    this.health = health;
    if (health <= 0){
      throw new IllegalArgumentException("Health can not be less than 0");
    }
    this.score = score;
    this.gold = gold;
    this.inventory = inventory;
  }

  public String getName() {
    return name;
  }

  public void addHealth(int health){
    this.health += health;

  }

  public int getHealth() {
    return health;
  }

  public void addScore(int points){
    this.score += points;
  }

  public int getScore() {
    return score;
  }

public void addGold(int gold){
    this.gold += gold;
}

  public int getGold() {
    return gold;
  }

  public void addInventory(String item){
    inventory.add(item);
  }

  public List<String> getInventory() {
    return inventory;
  }
}
