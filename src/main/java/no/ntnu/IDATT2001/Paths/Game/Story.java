package no.ntnu.IDATT2001.Paths.game;

import java.util.Collection;
import java.util.Map;

public class Story {
  private String title;
  private Passage openingPassage;
  private Map<Link, Passage> passages;

  public Story(String title, Passage openingPassage,  Map<Link, Passage> passages) {
    this.title = title;
    this.openingPassage = openingPassage;
    this.passages = passages;
  }

  public String getTitle() {
    return title;
  }

  public Passage getOpeningPassage() {
    return openingPassage;
  }

  //skjønte ikke helt
  public boolean addPassage(Passage passage){


  }


  public Passage getPassage(Link link){
    return passages.get(link);
  }

  public Collection<Passage> getPassages(){
    return passages.values();

  }
}
