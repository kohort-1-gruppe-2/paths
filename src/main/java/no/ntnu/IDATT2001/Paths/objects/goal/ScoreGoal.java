package no.ntnu.IDATT2001.Paths.objects.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * Represents a score goal. A score goal is a goal
 * that expects a minimum amount of points in the score.
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class ScoreGoal implements Goal {

  private int minimumPoints;

  //TODO: exception handling¨

  /**
   * method that sets the expected minimum points in the score of the player
   * @param minimumPoints minimum expected points in score
   */
  public void ScoreGoal(int minimumPoints){
    if (minimumPoints > 0){
    this.minimumPoints = minimumPoints;
    } else {
      throw new IllegalArgumentException("minimum points goal cannot be negative");
      }

  }


  /**
   * boolean method to check if the player has
   * fulfilled the set goal
   * @param p player that gets checked
   * @return true if the inventory goal is fulfilled and
   * false if the goal does not get fulfilled
   */
  public boolean isFulfilled(Player p){
    return (p.getScore() >= this.minimumPoints);
  }

}
