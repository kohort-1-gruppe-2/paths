package no.ntnu.IDATT2001.Paths.objects.action;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * ScoreAction class that implements the action class
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class ScoreAction implements Action {
  private int points;

  /**
   * method to change the players
   * score
   * @param points new amount of
   *               points in the
   *               players score
   * @return true or false depending
   * on the input being valid or not,
   * and the score being changed or not
   */
  public boolean ScoreAction(int points){
    if(points <= 0) return false;
    this.points = points;
    return true;
  }

  /**
   * method to change the state of
   * the player when used
   * @param player that is getting
   *              the state changed
   * @return true or false depending
   * on the input being valid or not,
   * and the state of the player being
   * changed or not
   */
  public boolean execute(Player player){
    if(player == null) return false;
    player.addScore(this.points);
    return true;
  }

  /***
   * method to get the amount of
   * points
   * @return integer that represents
   * the amount of points of the
   * player
   */
  public int getScoreAmount() {
    return this.points;
  }
}
