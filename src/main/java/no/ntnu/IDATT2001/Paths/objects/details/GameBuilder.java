package no.ntnu.IDATT2001.Paths.objects.details;

import no.ntnu.IDATT2001.Paths.objects.goal.Goal;

import java.util.List;

/**
 * Game builder class to build a Game objects
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class GameBuilder{

  public Player player;
  public Story story;
  public List<Goal> goals;

  /**
   * GameBuilder method that creates a
   * new instance of GameBuilder
   *
   * @return new instance of GameBuilder
   */
  public static GameBuilder newInstance() {
    return new GameBuilder();
  }

  private GameBuilder() {}

  /**
   * setPLayer method that sets the player
   * @param player to set
   * @return instance of GameBuilder
   */
  public GameBuilder setPlayer(Player player) {
    this.player = player;
    return this;
  }

  /**
   * setStory method to set the story
   * for the game.
   *
   * @param story object to set
   * @return instance of GameBuilder
   */
  public GameBuilder setStory(Story story) {
    this.story = story;
    return this;
  }

  /**
   * setGoals method that sets the goals for the game.
   *
   * @param goals list of goals to be set
   * @return instance of GameBuilder
   */
  public GameBuilder setGoals(List<Goal> goals) {
    this.goals = goals;
    return this;
  }

  /**
   * build builds a new Game object from the configured builder
   * @return new Game object
   */
  public Game build() {
    return new Game(this);
  }
}

