package no.ntnu.IDATT2001.Paths.objects.data;

import no.ntnu.IDATT2001.Paths.exceptions.CommaInNameException;
import no.ntnu.IDATT2001.Paths.exceptions.FileNotPathException;
import no.ntnu.IDATT2001.Paths.exceptions.InvalidFormatException;
import no.ntnu.IDATT2001.Paths.objects.action.*;
import no.ntnu.IDATT2001.Paths.objects.details.Link;
import no.ntnu.IDATT2001.Paths.objects.details.Passage;
import no.ntnu.IDATT2001.Paths.objects.details.Story;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.*;



/**
 * An object for handling data files. Can be used
 * for writing to a file for saving information, or
 * reading a file for retrieving information.
 *
 * @author Abhirami Karunananthan
 * @version 1.0.0
 */

public class DataController {

  private String fileName;
  private File file;

  /**
   * Constructor for a DataController. Takes the inputted
   * string, and turns it into the name of the file that is
   * being created. If the filename already exists, it creates a
   * new filename with numbers incremented after the original name.
   *
   * @param fileName The name of the file that should be created.
   * @throws IOException if file can't be created.
   */
  public DataController(String fileName) {
    String filePath = FileSystems.getDefault().getPath("src", "main", "resources", "no", "ntnu", "IDATT2001", "Paths", "objects", "data").toString();
    File filePathFile = new File(filePath);

    if (filePathFile.exists()) {
      file = new File(filePath + fileName + ".paths");
      if (file.exists()) {
        int i = 0;
        while (file.exists()) {
          i++;
          file = new File(filePath + fileName + i + ".paths");
        }
        this.fileName = fileName + i;
      } else {
        this.fileName = fileName;
      }
    } else {
      filePathFile.mkdir();
      file = new File(filePath + fileName + ".paths");
      if (file.exists()) {
        int i = 0;
        while (file.exists()) {
          i++;
          file = new File(filePath + fileName + i + ".paths");
        }
        this.fileName = fileName + i;
      } else {
        this.fileName = fileName;
      }
    }

  }


  /**
   * Constructor for a DataController. Takes the inputted
   * file and checks to see if it is a path file.
   *
   * @param inputFile The file that should be handled
   * @throws FileNotPathException If the file is not a path file
   */
  public DataController(File inputFile) throws FileNotPathException {
    String inputFileName = inputFile.getName();
    String fileExtension = inputFileName.substring(inputFileName.lastIndexOf('.') + 1);

    if (fileExtension.equals("paths")) {
      this.file = inputFile;
      this.fileName = inputFileName;
    } else {
      throw new FileNotPathException("File is not of file type 'paths'");
    }
  }

  /**
   * Returns the file.
   *
   * @return the file
   */

  public File getFile() {
    return file;
  }

  /**
   * Returns the file's name
   *
   * @return name of file
   */
  public String getFileName() {
    return fileName;
  }

  /**
   * Write a story's information into a file. First line in the
   * file is the name of the Story, then a line with space followed by
   * information about the passages with a space line between them
   * The first passage gets the role as the openingPassage
   * <p>
   * a new passage starts with "::" then the title
   * The next line includes the content of the passage
   * The last lines of the block lists the links and possible actions,
   * each link gets it own line, with the format [text](reference){action}
   * The block ends with an empty line
   *
   * @param story the story that should be written into the file
   * @return true if writing to the file was successful, false if not
   * @throws IOException if unable to write to file
   */
  public boolean writeStoryToFile(Story story) throws IOException {
    FileWriter fileWriter = new FileWriter(file);
    ArrayList<String> lineArrayList = new ArrayList<>();
    Action GoldAction = new GoldAction();
    Action HealthAction = new HealthAction();
    Action InventoryAction = new InventoryAction();
    Action ScoreAction = new ScoreAction();

    lineArrayList.add(story.getTitle());
    lineArrayList.add("");

    lineArrayList.add("::" + story.getOpeningPassage().getTitle());
    lineArrayList.add(story.getOpeningPassage().getContent());
    for (Link link : story.getOpeningPassage().getLinks()) {
      lineArrayList.add("[" + link.getText() + "](" + link.getReference() + ")");
      if (!link.getActions().isEmpty()) {
        for (Action action : link.getActions()) {
          lineArrayList.add("{" + action + "}");
        }
      }
    }
    lineArrayList.add("");

    for (Passage passage : story.getPassages()) {
      lineArrayList.add("::" + passage.getTitle());
      lineArrayList.add(passage.getContent());
      for (Link link : passage.getLinks()) {
        lineArrayList.add("[" + link.getText() + "](" + link.getReference() + ")");
      }
      lineArrayList.add("");
    }

    String finalString = "";
    for (String line : lineArrayList) {
      finalString += line + "\n";
    }

    fileWriter.write(finalString);
    fileWriter.close();
    return true;
  }

  /**
   * Reads this file's information and creates a story that corresponds
   * to the file. It reads the file line for line and creates passages
   * in story
   *
   * @return a stoy object created from the file's information
   * @throws FileNotFoundException  if the file does not exist in the directory
   * @throws InvalidFormatException if the information in the file representing the passage's class doesn't exist,
   * or if file is empty
   * @throws CommaInNameException  if the story name contains a comma
   */


  public Story readStoryFromFile() throws FileNotFoundException, InvalidFormatException, CommaInNameException {
    StringBuilder info = new StringBuilder();
    ArrayList<Passage> passageArrayList = new ArrayList<>();

    if (!file.exists()) {
      throw new FileNotFoundException("The file does not exist in the directory");
    }

    Scanner reader = new Scanner(file);

    while (reader.hasNextLine()) {
      info.append(reader.nextLine()).append("\n");
    }
    reader.close();

    if (info.length() < 4) {
      throw new InvalidFormatException("Invalid story format. At least one passage is required.");
    }

    String[] rawPassages = info.toString().split("::");
    String storyTitle = rawPassages[0].trim();
    if (storyTitle.contains(",")) {
      throw new CommaInNameException("There should not be comma in the title of the story");
    }
    String[] openingPassageContent = rawPassages[1].split("\n");
    Passage openingPassage = readPassage(openingPassageContent);

    Map<Link, Passage> passages = new HashMap<>();

    for (int i = 2; i < rawPassages.length; i++) {
      String rawPassage = rawPassages[i].trim();
      if (!rawPassage.isEmpty()) {
        String[] lines = rawPassage.split("\n");
        String[] passageContent = Arrays.copyOfRange(lines, 0, lines.length);
        Passage passage = readPassage(passageContent);
        passages.put(new Link(passage.getTitle(), passage.getTitle(), new ArrayList<>()), passage);
      }
    }
    Story story = new Story(storyTitle, openingPassage, passages);

    return story;
  }

  public Passage readPassage(String[] passage) throws InvalidFormatException {
    String passageTitle = passage[0];
    String content = passage[1];
    ArrayList<Link> links = new ArrayList<>();

    for (int i = 2; i < passage.length; i++) {
      String[] restLine = passage[i].split("\n");

      for (String restLines : restLine) {
        ArrayList<Action> listOfActions = new ArrayList<>();

        if (restLines.contains("{")) {
          String[] action = restLines.split("\\[|\\]|\\(|\\)|\\{|\\}");
          String[] nonEmptyArrayAction = Arrays.stream(action)
                  .filter(s -> !s.isEmpty())
                  .toArray(String[]::new);

          for (int j = 2; j < nonEmptyArrayAction.length; j++) {
            String[] actionType = nonEmptyArrayAction[j].split(":");
            String type = actionType[0].trim().toLowerCase();

            switch (type) {
              case "gold action":
                GoldAction goldAction = new GoldAction();
                goldAction.GoldAction(Integer.parseInt(actionType[1].trim()));
                listOfActions.add(goldAction);
                break;
              case "health action":
                HealthAction healthAction = new HealthAction();
                healthAction.HealthAction(Integer.parseInt(actionType[1].trim()));
                listOfActions.add(healthAction);
                break;
              case "inventory action":
                InventoryAction inventoryAction = new InventoryAction();
                inventoryAction.InventoryAction(actionType[1].trim());
                listOfActions.add(inventoryAction);
                break;
              case "score action":
                ScoreAction scoreAction = new ScoreAction();
                scoreAction.ScoreAction(Integer.parseInt(actionType[1].trim()));
                listOfActions.add(scoreAction);
                break;
              default:
                throw new InvalidFormatException("The four action types are gold action, health action, inventory action and score action");
            }
          }
          String[] nonEmptyArray = Arrays.stream(action)
                  .filter(s -> !s.isEmpty())
                  .toArray(String[]::new);
          Link link = new Link(nonEmptyArray[0], nonEmptyArray[1], listOfActions);
          links.add(link);
        } else {
         String[] splitRestLines =  restLines.trim().split("[\\[\\]\\(\\)]");
          String[] nonEmptyArray = Arrays.stream(splitRestLines)
                  .filter(s -> !s.isEmpty())
                  .toArray(String[]::new);
          Link link = new Link(nonEmptyArray[0], nonEmptyArray[1], new ArrayList<>());
          links.add(link);
        }
      }
    }

    return new Passage(passageTitle, content, links);

  }
}



