package no.ntnu.IDATT2001.Paths.objects.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

import java.util.List;

/**
 * Represents an inventory goal. An inventory goal is a goal
 * that expects a minimum amount of inventory.
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class InventoryGoal implements Goal{

  private List<String> mandatoryItems;


  /**
   * method that sets the expected items in the players inventory
   * @param mandatoryItems minimum expected items in the inventory
   */
  public void InventoryGoal(List<String> mandatoryItems) {
    this.mandatoryItems = mandatoryItems;
  }

  /**
   * boolean method to check if the player has
   * fulfilled the set goal
   * @param p player that gets checked
   * @return true if the inventory goal is fulfilled and
   * false if the goal does not get fulfilled
   */
  //TODO: exception handling
  public boolean isFulfilled(Player p) {
    return p.getInventory().size() >= this.mandatoryItems.size();
  }
}
