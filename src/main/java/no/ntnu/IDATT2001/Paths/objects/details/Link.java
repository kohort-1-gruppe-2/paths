package no.ntnu.IDATT2001.Paths.objects.details;

import no.ntnu.IDATT2001.Paths.objects.action.Action;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;


/**
 * The link class creates a
 * link, which makes it possible
 * to go from one passage to another.
 * The links binds
 * the different parts of a story
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class Link{
  private final String text;
  private final String reference;
  private final List<Action> actions;

  /**
   * This is a constructor for the
   * link class
   * @param text is a describing
   *            text for a choice or an action
   *            in the story.
   * @param reference is an unique string that
   *                  references to the specific
   *                  passage, and can be seen as
   *                  the title of the passage
   * @param actions is a list of specific objects
   *               that makes it possible to
   *                influence the characteristics
   *                of a player
   */
  public Link(String text, String reference, List<Action> actions) {
    this.text = text;
    this.reference = reference;
    this.actions = actions;
  }

  /**
   * Link creates a Link object based on the LinkBuilder.
   *
   * @param linkBuilder object to construct the Link from
   *
   * @throws NullPointerException when text is empty, or reference is empty
   * @throws IllegalArgumentException when reference or link contains special characters
   */
  public Link(LinkBuilder linkBuilder) {
    Pattern p = Pattern.compile("[@#$%&*:()_+=|<>{}\\[\\]\n~]", Pattern.CASE_INSENSITIVE);

    if (linkBuilder.text.isEmpty()) {
      throw new NullPointerException("Text cannot be null");
    } else if (p.matcher(linkBuilder.text).find()) {
      throw new IllegalArgumentException("Link text cannot contain special characters!");
    } else if (linkBuilder.reference.isEmpty()) {
      throw new NullPointerException("Reference cannot be null");
    } else if (p.matcher(linkBuilder.reference).find()) {
      throw new IllegalArgumentException("Link reference cannot contain special characters!");
    }

    this.text = linkBuilder.text.trim();
    this.reference = linkBuilder.reference.trim();
    this.actions = linkBuilder.actions;
  }

  /**
   * get method to get the text
   * @return string of text
   */
  public String getText() {
    return text;
  }

  /**
   * get method to get the reference
   * @return string of reference
   */
  public String getReference() {
    return reference;
  }

  /**
   * get method to get actions
   * @return list of actions
   */
  public List<Action> getActions() {
     return actions;
  }


  /**
   * toString method for the link class
   * @return tostring of text, reference
   * and actions
   */
  @Override
  public String toString() {
    return "text: " + text +
            "reference: " + reference +
            "actions: " + actions;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Link link = (Link) o;
    return Objects.equals(text, link.text) &&
            Objects.equals(reference, link.reference);
  }

  @Override
  public int hashCode() {
    return Objects.hash(text, reference);
  }
}



