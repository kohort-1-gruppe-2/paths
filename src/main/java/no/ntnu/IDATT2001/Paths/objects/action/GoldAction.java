package no.ntnu.IDATT2001.Paths.objects.action;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * GoldAction class that implements
 * the action class
 *
 * @author Abhirami Karunananthan
 *  @version 1.1.0
 */
public class GoldAction implements Action {
  private int gold;

  /**
   * method to change the players
   * gold holdings
   * @param gold new amount of gold
   *             in the players gold
   *             holding
   * @return true or false depending
   * on all the input being valid or
   * not, and the gold holding being
   * changed or not
   */

  public boolean GoldAction(int gold) {
    if (gold <= 0) {
      return false;
    }
    this.gold = gold;
    return true;
  }



  /**
   * method to change the state of the
   * player when used
   * @param player that is getting the
   *              state changed
   * @return true or false depending
   * on the input being valid or not,
   * and the state of the player being
   * changed or not
   */
  public boolean execute(Player player){
    if(player == null) return false;
    player.addGold(this.gold);
    return true;
  }

  /***
   * method to get the amount of
   * gold action
   * @return integer that represents
   * the amount of gold
   */
  public int getGoldAmount() {
    return this.gold;
  }

}
