package no.ntnu.IDATT2001.Paths.objects.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * Represents a gold goal. A gold goal is a goal
 * that expects a minimum amount of gold in the
 * gold holding.
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class GoldGoal implements Goal{

  private int minimumGold;


  /**
   * method that sets the minimum gold goal of the player
   * @param minimumGold the minimum gold goal value
   */
  public void GoldGoal(int minimumGold){
    if (minimumGold > 0) {
      this.minimumGold = minimumGold;
    } else {
      throw new IllegalArgumentException("minimum gold value cannot be negative");
    }
  }

  /**
   * boolean method to check if the player has
   * fulfilled the set goal
   * @param p player that gets checked
   * @return true if the gold goal is fulfilled and
   * false if the goal does not get fulfilled
   */
  public boolean isFulfilled(Player p){
    return p.getGold() >= this.minimumGold;
  }
}
