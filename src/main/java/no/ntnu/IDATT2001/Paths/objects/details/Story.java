package no.ntnu.IDATT2001.Paths.objects.details;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Story class that consists of an interactive
 * and non-linear narrative made of a collection
 * of passages
 *
 * @author Abhirami Karunananthan
 * '@version 1.1.2
 */
public final class Story {
  private final String title;
  private final Map<Link, Passage> passages;
  private final Passage openingPassage;

  public Story(String title, Passage openingPassage, Map<Link, Passage> passages) {
    if (title.isEmpty()) {
      throw new NullPointerException("There has to be a title to the story");
    }
    this.title = title;
    if (openingPassage == null) {
      throw new NullPointerException("There has to be an openings passage");
    }
    this.openingPassage = openingPassage;

    this.passages = passages;
  }

  /**
   * Constructs a Story object using the provided StoryBuilder.
   *
   * @param storyBuilder The StoryBuilder instance used to build the Story object.
   */
  public Story(StoryBuilder storyBuilder) {
    if (storyBuilder.title.isEmpty()) {
      throw new NullPointerException("title cannot be null");
    } else if (storyBuilder.openingPassage == null) {
      throw new NullPointerException("Opening passage cannot be null");
    }
    this.title = storyBuilder.title.trim();
    this.openingPassage = storyBuilder.openingPassage;
    this.passages = (HashMap<Link, Passage>) storyBuilder.passages;
  }

  /**
   * get method to get the title of the story
   *
   * @return title of the story
   */
  public String getTitle() {
    return title;
  }

  /**
   * get method to get the first passage in the
   * passage collection
   *
   * @return the first passage
   */
  public Passage getOpeningPassage() {
    return openingPassage;
  }


  /**
   * Adds a passage to the story.
   *
   * @param passage the passage to add to the story.
   * @return true if the passage was added successfully,
   * false if a passage with the same title already
   * exists in the story.
   * @throws NullPointerException if passage is null.
   */
  public boolean addPassage(Passage passage) {
    String passageTitle = passage.getTitle();
    Link link = new Link(passageTitle, passageTitle, new ArrayList<>());
    Passage existingPassage = passages.get(link);
    if (existingPassage != null) {
      return false;
    } else {
      passages.put(link, passage);
      return true;
    }
  }


  /**
   * get method to get a specific passage
   *
   * @param link the that matches to the wanted passage
   * @return the wanted passage
   */
  public Passage getPassage(Link link) {
    return passages.get(link);
  }

  /**
   * get method to get all the passages in the story
   *
   * @return all the passages in the story
   */
  public Collection<Passage> getPassages() {
    return passages.values();
  }

  /**
   * Removes a chosen passage from passages. It can
   * not be removed if there are other passages that
   * links to it.
   *
   * @param link the link of the passage to be removed
   * @return {@code true} if a passage with the specified
   * link was found and removed,
   * {@code false} otherwise (e.g., if the link does
   * not exist or there are other passages that links
   * to it.
   */
  public boolean removePassage(Link link) {
    Passage passageToRemove = passages.entrySet().stream()
            .filter(entry -> entry.getKey().equals(link))
            .map(Map.Entry::getValue)
            .findFirst()
            .orElse(null);

    if (passageToRemove != null) {
      boolean hasLink = passages.values().stream()
              .anyMatch(passage -> passage.hasLinks(link));

      if (hasLink) {
        return false;
      }

      passages.remove(link);
      return true;
    }

    return false;
  }







  /**
   * Returns a List of all broken links in the Map
   * of passages.
   *
   * @return a List of Link objects representing
   * broken links in the Map of passages
   */
  public List<Link> getBrokenLinks() {
    return passages.values().stream()
            .flatMap(p -> p.getLinks().stream()
                    .filter(l -> !passages.containsKey(l))
            )
            .distinct()
            .collect(Collectors.toList());
  }
}