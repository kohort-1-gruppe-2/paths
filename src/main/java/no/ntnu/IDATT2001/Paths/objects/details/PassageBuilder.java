package no.ntnu.IDATT2001.Paths.objects.details;


import java.util.ArrayList;
import java.util.List;

/**
 * Passage builder class to build a Passage objects
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class PassageBuilder {
  public String title;
  public String content;
  public List<Link> links = new ArrayList<>();

  /**
   * newInstance method creates
   * a new instance of PassageBuilder.
   *
   * @return new PassageBuilder instance.
   */
  public static PassageBuilder newInstance() {
    return new PassageBuilder();
  }

  private PassageBuilder() {}

  /**
   * SsetTitle method sets the
   * title of the passage.
   *
   * @param title title of the passage.
   * @return instance of passageBuilder
   */
  public PassageBuilder setTitle(String title) {
    this.title = title;
    return this;
  }

  /**
   * setContent method sets
   * the content of the passage.
   *
   * @param content content of the passage.
   * @return instance of passageBuilder
   */
  public PassageBuilder setContent(String content) {
    this.content = content;
    return this;
  }

  /**
   * setLink method sets the
   * links of the passage.
   *
   * @param links links of the passage.
   * @return instance of passageBuilder
   */
  public PassageBuilder setLinks(List<Link> links) {
    this.links = links;
    return this;
  }
  /**
   * build builds a new Passage object from the configured builder
   * @return new Passage object
   */
  public Passage build() {
    return new Passage(this);
  }
}

