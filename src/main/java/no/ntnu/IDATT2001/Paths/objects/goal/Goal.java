package no.ntnu.IDATT2001.Paths.objects.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * Interface for getting the type of goal
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public interface Goal {

  /**
   * boolean method to check if the player has
   * fulfilled the set goals
   * @param p player that gets checked
   * @return true if the goal is fulfilled and
   * false if the goals does not get fulfilled
   */
  public boolean isFulfilled(Player p);
}


