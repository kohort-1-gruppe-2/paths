package no.ntnu.IDATT2001.Paths.objects.details;

import no.ntnu.IDATT2001.Paths.objects.goal.Goal;

import java.util.List;

/**
 * Represents a game class. A
 * game is a facade
 * for the path game, it connects
 * the player to a story
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class Game {
  private final Player player;
  private final Story story;
  private final List<Goal> goals;

  /**
   * constructor for the game class
   * @param player is the person
   *               playing the
   *               paths game
   * @param story is the players
   *             passages used
   * @param goals is a list of
   *              special object
   *              that indicates
   *              the wanted
   *              achievements by
   *              the player
   */
  public Game(Player player, Story story, List<Goal> goals) {
    if(player == null) {throw new NullPointerException("There has to be a player");
    }
    this.player = player;
    if(story == null){
      throw new NullPointerException("There has to be a story");
    }
    this.story = story;
    if(goals.isEmpty()) {
      throw new NullPointerException("The players goals must be added ");
    }
    this.goals = goals;
  }

  /**
   * Game creates a Game object based on the GameBuilder.
   *
   * @param gameBuilder object to construct the Game from.
   * @throws NullPointerException when the player or story is null.
   */
  public Game(GameBuilder gameBuilder) {
    if (gameBuilder.player == null) {
      throw new NullPointerException("Player cannot be null");
    } else if (gameBuilder.story == null) {
      throw new NullPointerException("Story cannot be null");
    } else {
      this.player = gameBuilder.player;
      this.story = gameBuilder.story;
      this.goals = gameBuilder.goals;
    }
  }


  /**
   * get method to get player
   * @return wanted player object
   */
  public Player getPlayer() {
    return player;
  }

  /**
   * get method to get story
   * @return story
   */
  public Story getStory() {
    return story;
  }

  /**
   * get method to get goals
   * @return goals
   */
  public List<Goal> getGoals() {
    return goals;
  }

  /**
   * method to get the first
   * passage of the story of
   * this game
   * @return opening passage
   */
  public Passage begin(){
    return story.getOpeningPassage();
  }

  /**
   * method to get the passage
   * that matches the given link
   * @param link is the link
   *            that matches the
   *             wanted passage
   * @return the wanted passage
   */
  public Passage go(Link link) {
    return story.getPassage(link);

  }
}
