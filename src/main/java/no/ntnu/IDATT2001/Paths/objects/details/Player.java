package no.ntnu.IDATT2001.Paths.objects.details;

import java.util.ArrayList;
import java.util.List;

/**
 * Player class that represents a player with
 * different characteristics that can get
 * affected in a story
 *
 * @author Abhirami Karunananthan
 * @version 1.1.2
 */
public class Player {
  private final String name;
  private int health;
  private int score;
  private int gold;
  private List<String> inventory;


  /**
   * constructor for Player class
   *
   * @param name      of the player, can not be
   *                  empty
   * @param health    is a number that represents
   *                  the players health, can not be
   *                  less than zero
   * @param score     number of points of the player,
   *                  can not be negative
   * @param gold      is amount gold the player have,
   *                  can not be negative
   * @param inventory list of all the items the
   *                  player has
   * @throws NullPointerException     if players name is blank
   * @throws IllegalArgumentException if health, score or gold is negative
   */
  public Player(String name, int health, int score, int gold, List<String> inventory) {
    if (!name.isEmpty()) {
      this.name = name;
    } else {
      throw new NullPointerException("the players name can not be blank");
    }
    if (health >= 0) {
      this.health = health;
    } else {
      throw new IllegalArgumentException("health can not be negative");
    }
    if (score >= 0) {
      this.score = score;
    } else {
      throw new IllegalArgumentException("score can not be negative");
    }
    if (gold >= 0) {
      this.gold = gold;
    } else {
      throw new IllegalArgumentException("gold can not be negative");
    }
    this.inventory = new ArrayList<>(inventory);
  }

  /**
   *Player method to create a player
   * object with the given builder
   *
   * @param playerBuilder builder object with
   *                      the values@
   *
   * @throws NullPointerException     if players name
   *                                  is blank
   * @throws IllegalArgumentException if health, score
   *                                  or gold is negative
   */
  Player(PlayerBuilder playerBuilder) {
    if (!playerBuilder.name.isEmpty()) {
      this.name = playerBuilder.name;
      } else {
      throw new NullPointerException("Name can not be empty");
      }
      if (playerBuilder.health > 0) {
        this.health = playerBuilder.health;
      } else {
        throw new IllegalArgumentException("Health can not be zero or negative");
      }
       if (playerBuilder.score > 0) {
         this.score = playerBuilder.score;
       } else {
         throw new IllegalArgumentException("Score can not be negative or zero");
       }
       if (playerBuilder.gold > 0) {
         this.gold = playerBuilder.gold;
       } else {
      throw new IllegalArgumentException("Gold can not be negative or zero");
      }
      this.inventory = playerBuilder.inventory;
    }



  /**
   * get method to get the name
   *
   * @return the players name
   */
  public String getName() {
    return name;
  }

  /**
   * method to change the health, health can not
   * add negative number or zero
   *
   * @param health wants it to change to
   * @throws IllegalArgumentException if negative or zero health is added
   */
  public void addHealth(int health) {
    if (health < 0) throw new IllegalArgumentException(" Can not add negative health");
    this.health += health;
  }

  /**
   * get method to get the health
   *
   * @return health
   */
  public int getHealth() {
    return health;
  }

  /**
   * method to add points to the score, can not
   * add negative number or zero
   *
   * @param points want to add
   * @throws IllegalArgumentException if negative or zero points are added
   */
  public void addScore(int points) {
    if (points <= 0) throw new IllegalArgumentException(" Can not add zero or negative points to the score");
    this.score += points;
  }

  /**
   * get method to get the score
   *
   * @return the score
   */
  public int getScore() {
    return score;
  }

  /**
   * method to add gold, can not add a negative
   * number or zero
   *
   * @param gold want added
   * @throws IllegalArgumentException if negtive or zero gold is added
   */
  public void addGold(int gold) {
    if (gold <= 0) throw new IllegalArgumentException(" Can not add zero or negative gold");
    this.gold += gold;
  }

  /**
   * get method to get the amount of gold
   *
   * @return amount of gold
   */
  public int getGold() {
    return gold;
  }

  /**
   * method to add item to inventory, can not be
   * an empty string
   *
   * @param item want added
   * @throws NullPointerException if empty item is added to inventory
   */
  public void addToInventory(String item) {
    if (item.isEmpty()) throw new NullPointerException("Can not add empty item to inventory");
    item = item.toLowerCase().trim();
    inventory.add(item);
  }

  /**
   * method to get list of item in the inventory
   *
   * @return list of inventory
   */
  public List<String> getInventory() {
    return inventory;
  }
}
