package no.ntnu.IDATT2001.Paths.objects.action;


import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * Interface action that represents
 * a coming change in the state of a
 * player
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public interface Action {

  /**
   * method to change the state of the player when used
   * @param player that is getting the state changed
   * @return true or false depending on all the input
   * being valid or not, and the state of the player
   * was changed or not
   */
  public boolean execute(Player player);
}
