package no.ntnu.IDATT2001.Paths.objects.details;

import no.ntnu.IDATT2001.Paths.objects.action.Action;

import java.util.ArrayList;
import java.util.List;

/**
 * LinkBuilder class to build
 * class used to construct instances
 * of the Link class.
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */


public class LinkBuilder {

  public String text;
  public String reference;
  public List<Action> actions = new ArrayList<>();

  /**
   * LinkBuilder method that creates a
   * new instance of LinkBuilder
   *
   * @return A new instance of LinkBuilder.
   */
  public static LinkBuilder newInstance() {
    return new LinkBuilder();
  }

  private LinkBuilder() {}

  /**
   * SetText method that sets
   * the text of the link.
   *
   * @param text text of the link.
   * @return instance of LinkBuilder
   */
  public LinkBuilder setText(String text) {
    this.text = text;
    return this;
  }

  /**
   * setReference method that
   * sets the reference of the link.
   *
   * @param reference reference of the link.
   * @return instance of the LinkBuilder
   */
  public LinkBuilder setReference(String reference) {
    this.reference = reference;
    return this;
  }

  /**
   * setAction method sets the actions associated with the link.
   *
   * @param actions associated with the link.
   * @return instance of LinkBuilder
   */
  public LinkBuilder setActions(List<Action> actions) {
    this.actions = actions;
    return this;
  }

  /**
   * build builds a new Link object from the configured builder
   * @return new Link object
   */
  public Link build() {
    return new Link(this);
  }
}


