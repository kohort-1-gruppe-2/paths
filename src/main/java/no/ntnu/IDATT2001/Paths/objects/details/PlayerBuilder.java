package no.ntnu.IDATT2001.Paths.objects.details;

import java.util.ArrayList;
import java.util.List;

/**
 * Player builder class to build a player objects with
 * different characteristics that can get
 * affected in a story
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class PlayerBuilder {
  String name;
  int health;
  int score;
  int gold;
  List<String> inventory = new ArrayList<>();

  /**
   * PlayerBuilder method that creates a
   * new instance of PlayerBuilder
   *
   * @return new instance of playerBuilder
   */
  private static PlayerBuilder newInstance() {
    return new PlayerBuilder();
  }

  private PlayerBuilder() {}

  /**
   * setName methos that sets the players name
   * @param name of the player
   * @return instance of PLayerBuilder
   */
  public PlayerBuilder setName(String name) {
    this.name = name;
    return this;
  }

  /**
   * setScore method that sets the players score
   * @param score of the player
   * @return instance of PlayerBuilder
   */
  public PlayerBuilder setScore(int score) {
    this.score = score;
    return this;
  }

  /**
   * setGold method that sets the players gold
   * @param gold of the player
   * @return instance of PlayerBuilder
   */
  public PlayerBuilder setGold(int gold) {
    this.gold = gold;
    return this;
  }

  /**
   * setHealth method that sets the players health
   * @param health of the player
   * @return instance of PlayerBuilder
   */
  public PlayerBuilder setHealth(int health) {
    this.health = health;
    return this;
  }

  /**
   * build builds a new player object from the configured builder
   * @return new player object
   */
  public Player build() {
    return new Player(this);
  }


}
