package no.ntnu.IDATT2001.Paths.objects.action;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
  * HealthAction class that
 * implements the action class
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class HealthAction implements Action {
  private int health;

  /**
   * method to change the players
   * health level
   *
   * @param health new health level
   *               the player is on
   * @return true or false depending
   * on the input being valid or not,
   * and the health level being changed
   * or not
   */
  public boolean HealthAction(int health) {
    if (health < 0) return false;
    this.health = health;
    return true;

  }

  /**
   * method to change the state of the
   * player when used
   *
   * @param player that is getting the
   *               state changed
   * @return true or false depending on
   * the input being valid or not, and
   * the state of the player being
   * changed or not
   */
  public boolean execute(Player player) {
    if (player == null) return false;
    player.addHealth(this.health);
    return true;
  }
}
