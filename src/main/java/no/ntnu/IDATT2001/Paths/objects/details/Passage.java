package no.ntnu.IDATT2001.Paths.objects.details;

import no.ntnu.IDATT2001.Paths.exceptions.CommaInNameException;

import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

/**
 * The passage class represents a passage
 * which is a smaller part of the story,
 * like a chapter. To go from one passage
 * to another, the links are used.
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class Passage {
  private final String title;
  private final String content;
  private final List<Link> links;

  /**
   * Constructor for the passage class
   *
   * @param title   is a describing string used
   *                as an identifier
   * @param content is a text that typically
   *                represents a chapter or part
   *                of a dialog
   * @param links   is what connects a passage to
   *                another passage
   */
  public Passage(String title, String content, List<Link> links) {
    if (title.contains(",")) {
      throw new CommaInNameException("Title cannot contain a ','");
    }

    if (title.isEmpty()) {
      throw new IllegalArgumentException("There has to be a title to the passage");
    }

    this.title = title;
    this.content = content;
    this.links = links;

  }
  /**
   * Constructs a Passage instance using the provided PassageBuilder.
   *
   * @param passageBuilder The PassageBuilder used to construct the Passage instance.
   */
  public Passage(PassageBuilder passageBuilder) {
    Pattern p = Pattern.compile("[@#$%&*:()_+=|<>{}\\[\\]\n~]", Pattern.CASE_INSENSITIVE);

    if (!passageBuilder.title.isEmpty()) {
      this.title = passageBuilder.title.trim();
    } else {
      throw new NullPointerException("Title can not be empty");
    }
    if (!p.matcher(passageBuilder.title).find()) {
      this.content = passageBuilder.content.trim();
    } else {
      throw new IllegalArgumentException("title can not contain special characters");
    }
     if (passageBuilder.content.isEmpty()) {
       throw new NullPointerException("Content can not be empty");
     } else if (p.matcher(passageBuilder.content).find()) {
       throw new IllegalArgumentException("content can not contain special characters");
     } else {
    this.links = passageBuilder.links;
    }
  }


  /**
   * get method to get title
   *
   * @return title of the passage
   */

  public String getTitle() {
    return title;
  }

  /**
   * get method to get content
   *
   * @return content of the passage
   */
  public String getContent() {
    return content;
  }


  /**
   * method to add a link to the list of links
   * to a passage
   *
   * @param link wants added
   * @return true if the link is added, false if
   * it already exists and is not added
   */
  public boolean addLink(Link link) {
    if (links.contains(link)) {
      return false;
    } else {
      links.add(link);
      return true;
    }

  }

  /**
   * get method to get links
   *
   * @return list of links on passage
   */
  public List<Link> getLinks() {
    return links;
  }


  /**
   * method that checks if a chosen link exists
   * on the passage
   *
   * @param link wants to get checked
   * @return true if the link is a part of the
   * passage and false if it does not exist on
   * the passage
   */
  public boolean hasLinks(Link link) {
    return links.contains(link);
  }


  /**
   * equals method to compare two objects
   *
   * @param o is the object to compare with
   * @return true if the objects are equal and
   * false if it is not equal
   */
  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Passage passage = (Passage) o;
    return Objects.equals(title, passage.title) &&
            Objects.equals(content, passage.content) &&
            Objects.equals(links, passage.links);
  }




  /**
   * method to generate the hash values of
   * an object
   *
   * @return an integer value
   */
  @Override
  public int hashCode() {
    return Objects.hash(title, content, links);
  }
}

