package no.ntnu.IDATT2001.Paths.objects.goal;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * Represents a health goal. A health goal is a goal
 * that expects a minimum amount of health value.
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class HealthGoal implements Goal {

  private int minimumHealth;

  //TODO: exception handling

  /**
   * method that sets the minimum health goal value of the player
   * @param minimumHealth minimum health goal value
   */
  public void healthGoal(int minimumHealth){
    if(minimumHealth > 0) {
      this.minimumHealth = minimumHealth;
    } else throw new IllegalArgumentException("minimum health goal cannot be negative");
  }

  //TODO: exception handling
  /**
   * boolean method to check if the player has
   * fulfilled the set goal
   * @param p player that gets checked
   * @return true if the health goal is fulfilled and
   * false if the goal does not get fulfilled
   */
  public boolean isFulfilled(Player p){
    return p.getHealth() >= this.minimumHealth;
  }

}
