package no.ntnu.IDATT2001.Paths.objects.action;

import no.ntnu.IDATT2001.Paths.objects.details.Player;

/**
 * InventoryAction class that
 * implements the action class
 *
 * @author Abhirami Karunananthan
 * @version 1.1.0
 */
public class InventoryAction implements Action{

  private String item;


  /**
   * method to add an item to the players
   * inventory
   * @param item new item that is added
   *             to the inventory
   * @return true or false depending on
   * the input being valid or not, and
   * the inventory being changed or not
   */
  public boolean InventoryAction(String item){
    if(item.isEmpty()) return false;
    this.item = item.toLowerCase().trim();
    return true;
  }

  /**
   * method to change the state of the
   * player when used
   * @param player that is getting the
   *              state changed
   * @return true or false depending on
   * the input being valid or not, and
   * the state of the player being
   * changed or not
   */
  public boolean execute(Player player){
    if(player == null) return false;
    player.addToInventory(this.item);
    return true;
  }
}
