package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.game.Player;

public class InventoryAction implements Action{

  private String item;

  public void InventoryAction(String item){
    this.item = item;
  }

  public void execute(Player player){
    player.addInventory(this.item);
  }
}
