package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.game.Player;

public class GoldAction implements Action {
  private int gold;

  public void GoldAction(int gold){
    this.gold = gold;
  }

  public void execute(Player player){
    player.addGold(this.gold);
  }
}
