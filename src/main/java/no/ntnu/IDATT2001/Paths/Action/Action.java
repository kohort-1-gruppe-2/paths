package no.ntnu.IDATT2001.Paths.action;


import no.ntnu.IDATT2001.Paths.game.Player;

public interface Action {
  public void execute(Player player);
}
