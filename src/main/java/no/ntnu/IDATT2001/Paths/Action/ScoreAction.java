package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.game.Player;

public class ScoreAction implements Action {
  private int points;


  public void ScoreAction(int points){
    this.points = points;
  }

  public void execute(Player player){
    player.addScore(this.points);
  }
}
