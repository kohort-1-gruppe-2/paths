package no.ntnu.IDATT2001.Paths.action;

import no.ntnu.IDATT2001.Paths.game.Player;

public class HealthAction implements Action{
  private int health;

  public void HealthAction(int health){
    this.health = health;

  }
  public void execute(Player player){
    player.addHealth(this.health);
  }
}
