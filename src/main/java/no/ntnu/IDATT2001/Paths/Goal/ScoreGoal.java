package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.game.Player;

public class ScoreGoal implements Goal{

  private int minimumPoints;

  public void ScoreGoal(int minimumPoints){
    this.minimumPoints = minimumPoints;
  }

  public boolean isFulfilled(Player player){
    return (player.getScore() >= this.minimumPoints);
  }

}
