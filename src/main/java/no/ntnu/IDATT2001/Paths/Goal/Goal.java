package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.game.Player;

public interface Goal {
  public boolean isFulfilled(Player player);
}


