package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.game.Player;

public class HealthGoal implements Goal{

  private int minimumHealth;

  public void healthGoal(int minimumHealth){
    this.minimumHealth = minimumHealth;
  }

  public boolean isFulfilled(Player player){
    return player.getHealth() >= this.minimumHealth;
  }

}
