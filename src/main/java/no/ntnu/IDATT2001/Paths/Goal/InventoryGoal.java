package no.ntnu.IDATT2001.Paths.goal;
import no.ntnu.IDATT2001.Paths.game.Player;

import java.util.List;

public class InventoryGoal {

  private List<String> mandatoryItems;

  public void InventoryGoal(List<String> mandatoryItems){
    this.mandatoryItems = mandatoryItems;
  }

  public boolean isFulfilled(Player player){
    return player.getInventory().size() >= this.mandatoryItems.size();
  }
}
