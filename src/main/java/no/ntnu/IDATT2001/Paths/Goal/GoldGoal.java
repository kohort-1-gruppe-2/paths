package no.ntnu.IDATT2001.Paths.goal;

import no.ntnu.IDATT2001.Paths.game.Player;

public class GoldGoal {

  private int minimumGold;

  public void GoldGoal(int minimumGold){
    this.minimumGold = minimumGold;
  }

  public boolean isFulfilled(Player player){
    return player.getGold() >= this.minimumGold;
  }
}
