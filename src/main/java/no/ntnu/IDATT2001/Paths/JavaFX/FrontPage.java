package no.ntnu.IDATT2001.Paths.JavaFX;

import javafx.scene.control.Button;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class FrontPage extends VBox {
  // Add your JavaFX UI elements and functionality here

  public FrontPage() {
    // Create a button
    Button button = new Button("Import game from file");

    // Add the button to the VBox
    getChildren().add(button);

    // Add functionality to the button
    button.setOnAction(e -> {
      // Perform an action when the button is clicked
      System.out.println("Button clicked!");
    });
  }

}
