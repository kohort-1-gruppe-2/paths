package no.ntnu.IDATT2001.Paths.JavaFX;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Application extends javafx.application.Application {
  @Override
  public void start(Stage primaryStage) {
    FrontPage gameManager = new FrontPage();
    Scene scene = new Scene(gameManager, 400, 300);

    primaryStage.setScene(scene);
    primaryStage.show();
  }

  public static void main(String[] args) {
    launch(args);
  }
}