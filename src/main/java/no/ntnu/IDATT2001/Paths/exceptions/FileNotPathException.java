package no.ntnu.IDATT2001.Paths.exceptions;

/**
 * Exception class for when a given file does not
 * end with '.paths'
 *
 * @author  Abhirami Karunananthan
 * @version 1.0.0
 */
public class FileNotPathException extends Exception{

  /**
   * Constructs a new FileNotPathsException with the specified detail message.
   * The cause is not initialized, and may subsequently be initialized by a call to initCause.
   *
   * @param message   the detail message. The detail message is saved for later retrieval by the getMessage() method.
   */
  public FileNotPathException(String message){
    super(message);
  }

  /**
   * Constructs a new FileNotPathsException with the specified cause and a detail message of (cause==null ? null : cause.toString())
   * (which typically contains the class and detail message of cause).
   * This constructor is useful for exceptions that are little more than wrappers for other throwables
   * (for example, java.security.PrivilegedActionException).
   *
   * @param cause     the cause (which is saved for later retrieval by the getCause() method).
   *                  (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
   */
  public FileNotPathException(Throwable cause){
    super(cause);
  }

  /**
   * Constructs a new FileNotPathsException with the specified detail message, cause,
   * suppression enabled or disabled, and writable stack trace enabled or disabled.
   *
   * @param message      the detail message.
   * @param cause        the cause. (A null value is permitted, and indicates that the cause is nonexistent or unknown.)
   *                     enableSuppression – whether or not suppression is enabled or disabled
   *                     writableStackTrace – whether or not the stack trace should be writable
   */
  public FileNotPathException(String message, Throwable cause){
    super(message, cause);
  }



}
